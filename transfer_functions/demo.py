import numpy as np
import matplotlib.pyplot as plt
import single_neuron_simulations as sims # where we store the single cells simulation
import sys
sys.path.append('.../code/')
from my_graph import set_plot

## model parameters

membrane_params = {\
'El' : -60e-3,
'Cm': 200e-12,
'Gl' : 10e-9}

synaptic_params = {\
'Te' : 5e-3,
'Ti' : 5e-3,
'Ee' : 0e-3,
'Ei' : -80e-3,
'Qi' : 20e-9,
'Qe' : 2e-9}

network_params = {\
'Ntot'  : 10000,
'pconnec'  : 0.02,
'gei' : .2}

spiking_params = {\
'Vthre' : -50e-3,
'Vreset':-60e-3,
'Vpeak':0e-3, # estethic
'Trefrac':5e-3,
'a' : 0.,
'b' : 40e-12,
'tauw' : 600e-3,
'delta_v' : 2e-3}

# full params
P = dict(membrane_params.items()+network_params.items()+synaptic_params.items()+spiking_params.items())


def demo_plot(P=P, dt=0.1e-3, tstop=1000e-3, I0=150e-12, fe=10., fi=10.):

    t = np.arange(int(tstop/dt))*dt

    fig = plt.figure(figsize=(10,10))

    ax11 = plt.subplot2grid((6,2), (0, 0), rowspan=3)
    ax21 = plt.subplot2grid((6,2), (3,0))
    ax31 = plt.subplot2grid((6,2), (4,0))
    ax41 = plt.subplot2grid((6,2), (5,0))

    ax12 = plt.subplot2grid((6,2), (0, 1), rowspan=3)
    ax22 = plt.subplot2grid((6,2), (3,1))
    ax32 = plt.subplot2grid((6,2), (4,1))
    ax42 = plt.subplot2grid((6,2), (5,1))

    I = np.array([I0 if ( (tt>200e-3) and (tt<900e-3)) else 0 for tt in t])
    v, spikes = sims.adexp_sim(t, I, 0.*I, 0.*I, *sims.pseq_adexp(P))

    ax11.plot(1e3*t, 1e3*v, 'k-')
    ax21.plot(1e3*t, 1e12*I, 'g-')
    ax31.plot(1e3*t, 0*I, 'r-')
    ax41.plot(1e3*t, 0*I, 'b-')


    fe, fi = 10, 10
    Ge = sims.generate_conductance_shotnoise(fe, t, P['Ntot']*(1-P['gei'])*P['pconnec'], P['Qe'], P['Te'], g0=0, seed=0)
    Gi = sims.generate_conductance_shotnoise(fi, t, P['Ntot']*P['gei']*P['pconnec'], P['Qi'], P['Ti'], g0=0, seed=1)

    v, spikes = sims.adexp_sim(t, 0.*I, Ge, Gi, *sims.pseq_adexp(P))

    ax12.plot(1e3*t, 1e3*v, 'k-')
    ax22.plot(1e3*t, 0*I, 'g-')
    ax32.plot(1e3*t, 1e9*Ge, 'r-')
    ax42.plot(1e3*t, 1e9*Gi, 'b-')

    set_plot(ax12, ylabel='V (mV)', xticks_labels=[])
    set_plot(ax22, ylabel='I (pA)', xticks_labels=[])
    set_plot(ax32, ylabel='Ge (nS)', xticks_labels=[])
    set_plot(ax42, ylabel='Gi (nS)', xlabel='time (ms)')
    set_plot(ax11, ylabel='V (mV)', xticks_labels=[])
    set_plot(ax21, ylabel='I (pA)', xticks_labels=[])
    set_plot(ax31, ylabel='Ge (nS)', xticks_labels=[])
    set_plot(ax41, ylabel='Gi (nS)', xlabel='time (ms)')
    
    return fig


if __name__=='__main__':
    # let's have a look at its spiking behavior
    if len(sys.argv)>1:
        sims.generate_transfer_function(P, data_name=sys.argv[-1])
    else:
        demo_plot()
    plt.show()

