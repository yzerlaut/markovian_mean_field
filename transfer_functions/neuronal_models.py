"""
Loads the model with constants as previously published
if it already has the key (e.g. 'Gl'), it does not change it,
else it gives the default value
"""
import numpy as np

default_params = {\
'El' : -70e-3,
'Cm': 200e-12,
'Gl' : 10e-9,
'Vthre':-50e-3,
'Vreset':-60e-3,
'a' : 0.,
'b' : 0e-12,
'tauw' : 1e9,
'Trefrac':5e-3,
'delta_v' : 0}


def get_model_params(MODEL):

    ## we start with the passive parameters, by default
    ## they will be overwritten by some specific models
    ## (e.g. Adexp-Rs, Wang-Buszaki) and not by others (IAF, EIF)
    params = default_params

    # """ ======== INTEGRATE AND FIRE ============ """
    if MODEL=='IaF':
        # params by default are IaF
        params['name'] = 'IaF'
        params['name_for_tf'] = 'IaF'

    elif MODEL=='IaF_b_20':
        # to be used in the models where W is a variable!!!
        params['name'] = 'IaF+b20'
        params['b'] = 20e-12
        params['tauw'] = 500e-3
        # so we use the stationary TF of IaF
        params['name_for_tf'] = 'IaF'

    elif MODEL=='IaF_b_200':
        # to be used in the models where W is a variable!!!
        params['name'] = 'sfaLIF, b=200pA'
        params['b'] = 200e-12
        params['tauw'] = 500e-3
        # so we use the stationary TF of IaF
        params['name_for_tf'] = 'IaF'

    elif MODEL=='AdExp_RS': # default for all Adexp models
        params['b'] = 50e-12          # (pA->A)   : increment of adaptation
        params['tauw'] = 500e-3          # (ms)   : time constant of adaptation
        params['delta_v'] = 1e-3           # (mV)   : steepness of exponential approach to threshold
        params['name']='AdExp-RS'
        params['name_for_tf']='AdExp_RS'
        
    elif MODEL=='cell1':
        # params by default are IaF
        params['name'] = 'cell1'
        params['name_for_tf'] = 'cell1'

    else:
        params = None
        print '==========> ||neuron model not recognized|| <================'

    return params

all_models = ['IaF', 'IaF+b20', 'IaF+b40', 'AdExp-RS', 'cell1']

import argparse
import pprint

if __name__=='__main__':

    str_all_models = '--> '
    for m in all_models: str_all_models += m+'\n --> '

    parser=argparse.ArgumentParser(description=
            """
            defines the NEURONAL parameters
            from the following models:
            """+str_all_models+'ALL (to print the values of all models)',
            formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("Neuron_Model",help="Choose a network model")

    args = parser.parse_args()

    if args.Neuron_Model=='ALL':
        for m in all_models:
            p = get_model_params(m)
            print "=============================================="
            print "===----", p['name'], "-----==========="
            print "=============================================="
            pprint.pprint(p)
    else:
            p = get_model_params(args.Neuron_Model)
            print "=============================================="
            print "===----", p['name'], "-----==========="
            print "=============================================="
            pprint.pprint(p)

