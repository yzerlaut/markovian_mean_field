import numpy as np

default_params = {\
'Ntot': 10000,
'gei':0.2,
'pconnec':0.005,
'Qe':6e-9,
'Te':5e-3,
'Ee':0e-3,
'Qi':100e-9,
'Ti':5e-3,
'Ei':-80e-3,
'exc_drive':0.,
'fixed_Erev':-60e-3,#for current stim
'conductance':True}

def construct_balance(params,\
                     muV=-60e-3, El=-70e-3, gL=10e-9):
    """ what Qi value do you need to get the mean of fluct at rest ?"""
    # first excitatory afferent input to put bring to mean
    muV0 = -55e-3
    num = gL*(muV-El)
    denom = (1-params['gei'])*params['pconnec']*\
            params['Ntot']*params['Qe']*(params['Ee']-muV)
    Fe_aff = num/denom/params['Te']
    # then inhibitory weight to have 0-mean recurrent input
    a = params['Te']/params['Ti']
    b = (1-params['gei'])/params['gei']
    c = (params['Ee']-muV)/(muV-params['Ei'])
    Qi = params['Qe']*a*b*c
    return Fe_aff, Qi

def get_model_params(MODEL):

    ## we start with the passive parameters, by default
    ## they will be overwritten by some specific models
    ## (e.g. Adexp-Rs, Wang-Buszaki) and not by others (IAF, EIF)
    params = default_params

    # """ ======== INTEGRATE AND FIRE ============ """
    if MODEL=='MULLER2014':
        # params by default 
        params['name'] = 'network in Muller et al. 2014'

    elif MODEL=='CONFIG1':
        # params by default, good for CELL = cell 1 !!
        params['name'] = 'config1-conductance'
        params['Qe'] = 2e-9
        params['Ntot'] = 10000
        params['pconnec'] = 0.01
        Fe_aff, Qi = construct_balance(params,
                                       muV=-60e-3, El=-70e-3, gL=10e-9)
        params['Qi'] = Qi
        params['exc_drive'] = Fe_aff # not self sustained !!!
        print 'external drive, F=', Fe_aff, 'Hz'
        print 'inhibitory quantal, Qi=', 1e9*Qi, 'nS'
        params['ntwk_name_for_tf'] = 'CONFIG1'
        # to be associated with IaF
        params['NU0'] = 14.1 # fixed point of the dynamics (to start)

    elif MODEL=='CONFIG2':
        # good for CELL = IaF !!
        params['conductance'] = True
        params['name'] = 'config2-conductance'
        params['Qe'] = 2.1e-9
        params['Ntot'] = 5000
        params['pconnec'] = 0.02
        Fe_aff, Qi = construct_balance(params,
                                       muV=-65e-3, El=-70e-3, gL=10e-9)
        params['Qi'] = Qi
        params['exc_drive'] = Fe_aff # not self sustained !!!
        print 'external drive, F=', Fe_aff, 'Hz'
        print 'inhibitory quantal, Qi=', 1e9*Qi, 'nS'
        params['ntwk_name_for_tf'] = 'CONFIG2'
        # to be associated with cell2
        params['NU0'] = 4.5 # fixed point of the dynamics (to start)
        
    elif MODEL=='CONFIG3':
        # good for CELL = IaF_b200 !!
        params['conductance'] = True
        params['name'] = 'config2-conductance'
        params['Qe'] = 1e-9
        params['Ntot'] = 10000
        params['pconnec'] = 0.02
        Fe_aff, Qi = construct_balance(params,
                                       muV=-55e-3, El=-70e-3, gL=10e-9)
        params['Qi'] = Qi
        params['exc_drive'] = Fe_aff # not self sustained !!!
        print 'external drive, F=', Fe_aff, 'Hz'
        print 'inhibitory quantal, Qi=', 1e9*Qi, 'nS'
        params['ntwk_name_for_tf'] = 'CONFIG2'
        # to be associated with cell2
        params['NU0'] = 4.5 # fixed point of the dynamics (to start)
        
    elif MODEL=='CONFIG4':
        # good for RS & FS model
        params['conductance'] = True
        params['name'] = 'config2-conductance'
        params['Qe'] = 2e-9
        params['Qi'] = 10e-9
        params['Ntot'] = 10000
        params['pconnec'] = 0.02
        params['exc_drive'] = 2.
        params['ntwk_name_for_tf'] = 'CONFIG4'
        # to be associated with cell2
        params['NU0'] = 5. # fixed point of the dynamics (to start)
        
    elif MODEL=='CONFIG1-CURRENT':
        # params by default 
        params['name'] = 'config1-current'
        params['conductance'] = False
        params['Qe'] = 2e-9
        params['Ntot'] = 10000
        params['pconnec'] = 0.01
        params['Qi'] = 40e-9
        params['exc_drive'] = 2. # not self sustained !!!
        params['fixed_Erev'] = -60e-3

        # params['exc_drive'], params['Qi'] =\
        #         construct_balance_for_current_based(params, muV=-60e-3)
        # params['exc_drive'], params['Qi'] = 4.5, 1.5*params['Qi']
        params['ntwk_name_for_tf'] = 'CONFIG1-CURRENT'

    elif MODEL=='CONFIG1+b20':
        # params by default 
        params['name'] = 'config-1 for b=20pA'
        params['Qe'] = 2e-9
        params['Qi'] = 40e-9
        params['Ntot'] = 10000
        params['pconnec'] = 0.01
        params['exc_drive'] = 4. # not self sustained !!!
        params['ntwk_name_for_tf'] = 'CONFIG1'

    elif MODEL=='real':
        # params by default 
        params['name'] = 'realistic like config'
        params['Qe'] = 2e-9
        params['Qi'] = 20e-9
        params['Ntot'] = 10000
        params['pconnec'] = 0.05
        params['exc_drive'] = 0. # not self sustained !!!
        params['ntwk_name_for_tf'] = 'CONFIG1'

    else:
        params = None
        print '==========> ||neuron model not recognized|| <================'

    return params

all_models = ['MULLER2014', 'CONFIG1', 'CONFIG1-CURRENT', 'CONFIG1+b20']

import argparse
import pprint

if __name__=='__main__':

    str_all_models = '--> '
    for m in all_models: str_all_models += m+'\n --> '

    parser=argparse.ArgumentParser(description=
            """
            defines the NETWORK (connectivity) AND SYNAPTIC parameters
            from the following models:
            """+str_all_models+'ALL (to print the values of all models)',
            formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("Network_Model",help="Choose a network model")

    args = parser.parse_args()

    if args.Network_Model=='ALL':
        for m in all_models:
            p = get_model_params(m)
            print "=============================================="
            print "===----", p['name'], "-----==========="
            print "=============================================="
            pprint.pprint(p)
    else:
            p = get_model_params(args.Network_Model)
            print "=============================================="
            print "===----", p['name'], "-----==========="
            print "=============================================="
            pprint.pprint(p)
        

